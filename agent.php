<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <style type="text/css">

        #local {
            width: 160px;
            height: 120px;
        }

        #remote {
            width: 320px;
            height: 240px;
        }

        #local, #remote {
            border: 1px solid grey;
        }

    </style>
</head>

<body>

    <!-- remote video view -->
    <div id="remote"></div>

    <!-- local video preview -->
    <div id="local"></div>

    <!-- libraries needed for Assist SDK -->
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
    <script src="http://192.168.0.99:8080/gateway/adapter.js"></script>
    <script src="http://192.168.0.99:8080/gateway/csdk-sdk.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/assist-aed.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/shared-windows.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>



    <!-- load jQuery - helpful for DOM manipulation -->
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- control -->
    <script>

        <?php
        // since we know that the agent will require to be provisioned, we
        // can eagerly provision a session token on the page load rather
        // than do so lazily later

        // include the Provisioner class we created to do the provisioning
        require_once('Provisioner.php');
        $provisioner = new Provisioner;
        $token = $provisioner->provisionAgent(
            'agent1',
            '192.168.0.99',
            '.*');

        // decode the sessionID from the provisioning token
        $sessionId = json_decode($token)->sessionid;
        ?>

        var sessionId = '<?php echo $sessionId; ?>';
        console.log("SessionID: " + sessionId);

        var AgentModule = function (sessionId) {
            // Declare variables needed during the call
            var remote,
                local;

            // organise functions to do important tasks...
            cacheDom(); // cache the DOM elements we will need later during the call
            setClickHandlers();  // define what happens when our app's buttons are clicked
            bindAgentSdkCallbacks();  // set all of the agentSdk's call-back listeners
            linkUi(); //
            // ...then, with everything prepared and ready ...
            init(sessionId);  // init the library and register the agent to receive calls


            // caches the DOM elements we will need later during the call
            function cacheDom() {
                // extract native DOM elements - AgentSDK works with DOM elements directly
                remote = $('#remote')[0];
                local = $('#local')[0];
            }

            // add click handlers that determine what happens when agent clicks buttons, etc
            function setClickHandlers() {
                // ... no click handlers needed yet, but will added soon!
            }

            // set all of the agentSdk's call-back listeners
            function bindAgentSdkCallbacks(){
                // ... no sdk callbacks needed yet, but will added soon!
            }

            // links UI elements to their Agent SDK outlets
            function linkUi() {
                // video calling elements
                CallManager.setRemoteVideoElement(remote);
                CallManager.setLocalVideoElement(local);
            }

            // initialises using the Agent SDK CallManager
            function init(sessionId) {

                var gatewayUrl = 'http://192.168.0.99:8080';
                CallManager.init({
                    sessionToken: sessionId,
                    autoanswer: false,
                    username: 'agent1',
                    password: 'none',
                    agentName: 'Agent',
                    agentPictureUrl: gatewayUrl + '/assistserver/img/avatar.png',
                    url: gatewayUrl
                });
            }

        }(sessionId);

    </script>
</body>
</html>